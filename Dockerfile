FROM alpine

ARG CLUSTER_API_VERSION=v1.2.2
ARG KUBERNETES_RELEASE=v1.25.3

RUN apk add --no-cache curl jq bash
RUN curl -L https://github.com/kubernetes-sigs/cluster-api/releases/download/${CLUSTER_API_VERSION}/clusterctl-linux-amd64 -o /usr/local/bin/clusterctl
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_RELEASE}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/clusterctl /usr/local/bin/kubectl
